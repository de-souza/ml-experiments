from fastai.basics import torch, tensor, nn, mse, plt
from matplotlib import animation

# Create the mystery function

n = 100  # Number of points
x = torch.ones(n, 3)  # Create a 2-column array of length n filled with ones
x[:, 1].uniform_(-1.0, 1.0)  # Set the 2nd column to numbers from -1 to 1
x[:, 2] = x[:, 1].pow(2)  # Set the 1st column to the square of the 1st
a = tensor(-1., 0.5, 1.)  # Create a vector for the mystery function parmeters
noise = torch.rand(n) - 0.5
y = x @ a + 0.25 * noise  # Set the mystery function to x^a + random noise

print(f"Mystery function: y = {a[2]}*x² + {a[1]}*x + {a[0]}")

# Guess the mystery function parameters

a = nn.Parameter(tensor(0.0, 0.0, 0.0))  # Initial guess
lr = 3e-1  # Learning rate

def step():
    y_hat = x @ a  # Function resulting from the current guess
    loss = mse(y, y_hat)  # Calculate the loss function
    opt = torch.optim.SGD((a,), lr)  # Select stochastic gradient descent
    opt.zero_grad()  # Clear the old gradient left from last step
    loss.backward()  # Calculate the gradient (derivative of the loss)
    opt.step()  # Substract the lr * the gradient from the current guess

def animate(_):
    step()
    line.set_ydata(x @ a.detach())
    return (line,)

# Plot result

fig = plt.figure()
plt.scatter(x[:, 1], y, c="orange")
(line,) = plt.plot(x[:, 1], x @ a.detach(), marker="o", ls="")
animation.FuncAnimation(fig, animate, 100, interval=20, blit=True)
plt.show()

print(f"Guessed function: y = {a[2]}*x² + {a[1]}*x + {a[0]}")
