"""Linear regression using SGD and a single neuron."""
import torch
import pytorch_lightning as pl
import matplotlib.pyplot as plt


class LinearFit(pl.LightningModule):
    """Single-neuron system for linear regression."""

    def __init__(self):
        super().__init__()
        self.hidden = torch.nn.Linear(1, 1000)
        self.output = torch.nn.Linear(1000, 1)

    def forward(self, x):
        x = x.view(x.size(0), -1)
        x = self.hidden(x)
        x = torch.nn.functional.relu(x)
        return self.output(x)

    def training_step(self, batch, _):
        data_x, data_y = batch.t()
        data_y_hat = self(data_x)[:, 0]
        loss = torch.nn.functional.mse_loss(data_y_hat, data_y)
        tensorboard_logs = {"train_loss": loss}
        return {"loss": loss, "log": tensorboard_logs}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1, weight_decay=1e-4)

    def prepare_data(self):
        size = 2048
        noise = (torch.rand(size) - 0.5) / 8
        data_x = (torch.rand(size) - 0.5) * 4
        data_y = (-data_x.pow(2)).exp() * (16*data_x).sin() + noise
        self.data = torch.stack((data_x, data_y), dim=1)

    def train_dataloader(self):
        return torch.utils.data.DataLoader(self.data, batch_size=256)


class PlotCallback(pl.callbacks.Callback):
    """Callback for plotting the output during the training process."""

    def on_train_start(self, trainer, pl_module):
        plt.figure()
        plt.scatter(*pl_module.data.t(), c="orange")
        plt.ion()

    def on_epoch_end(self, trainer, pl_module):
        data_x = pl_module.data[:, 0]
        with torch.no_grad():
            data_y_hat = pl_module(data_x)
        lines = plt.gca().get_lines()
        if lines:
            lines[0].set_ydata(data_y_hat)
        else:
            plt.plot(data_x, data_y_hat, marker=".", ls="")
        plt.pause(0.01)

    def on_train_end(self, trainer, pl_module):
        plt.ioff()
        plt.show()


class PrintCallback(pl.callbacks.Callback):
    """Callback for printing the parameters during the training process."""

    @staticmethod
    def _print_params(pl_module):
        for key, val in pl_module.state_dict().items():
            print(f"{key}: {val}")

    def on_train_start(self, trainer, pl_module):
        print("Initial parameters:")
        self._print_params(pl_module)

    def on_train_end(self, trainer, pl_module):
        print("Final parameters:")
        self._print_params(pl_module)


def main():
    model = LinearFit()
    trainer = pl.Trainer(
        max_epochs=100,
        checkpoint_callback=False,
        callbacks=[PlotCallback()],
    )
    trainer.fit(model)


main()
